package modeltests

import (
	"log"
	"testing"
	"time"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/models"
	"gopkg.in/go-playground/assert.v1"
)

// TestGetHealthDataByUserId method tests getting all the health data by user
func TestGetHealthDataByUserId(t *testing.T) {
	err := createUserAndHealthDetailTable()

	if err != nil {
		log.Fatal(err)
	}

	healthData, err := seedOneUserAndOneHealthDetail()

	if err != nil {
		log.Fatal(err)
	}

	healthDataByUser, err := healthDataInstance.FindHealthDataByUserId(server.Db, healthData.Userid)

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, healthDataByUser.ID, healthData.ID)
	assert.Equal(t, healthDataByUser.Age, healthData.Age)
}

//TestGetHealthDataById method tests getting the health details by id
func TestGetHealthDataById(t *testing.T) {
	err := createUserAndHealthDetailTable()

	if err != nil {
		log.Fatal(err)
	}

	healthData, err := seedOneUserAndOneHealthDetail()

	if err != nil {
		log.Fatal(err)
	}

	healthDetails, err := healthDataInstance.FindHealthDetails(server.Db, healthData.ID)

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, healthDetails.ID, healthData.ID)
	assert.Equal(t, healthDetails.Age, healthData.Age)
}

//TestSaveHealthData method tests saving od health data by the user
func TestSaveHealthData(t *testing.T) {
	err := createUserAndHealthDetailTable()

	if err != nil {
		log.Fatal(err)
	}

	savedUser, err := userInstance.SaveUser(server.Db)

	if err != nil {
		log.Fatal(err)
	}

	healthData := models.HealthDetail{
		Userid:     savedUser.Id,
		Height:     34,
		Weight:     80,
		Age:        25,
		Bloodgroup: "A+",
		Createdat:  time.Now(),
		Updatedat:  time.Now(),
	}

	userHealthData, err := healthData.SaveData(server.Db)

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, healthData.Userid, userHealthData.Userid)
	assert.Equal(t, healthData.Height, userHealthData.Height)
}

//TestUpdateHealthData method tests updating of health data by the user
func TestUpdateHealthData(t *testing.T) {
	err := createUserAndHealthDetailTable()

	if err != nil {
		log.Fatal(err)
	}

	savedHealthData, err := seedOneUserAndOneHealthDetail()

	if err != nil {
		log.Fatal(err)
	}

	newHealthData := models.HealthDetail{
		Age:    45,
		Height: 100,
		Weight: 45,
	}

	updateHealthData, err := newHealthData.UpdateHealthDetail(server.Db, uint64(savedHealthData.ID))

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, updateHealthData.Age, newHealthData.Age)
	assert.Equal(t, updateHealthData.Height, newHealthData.Height)
}
