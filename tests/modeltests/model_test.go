package modeltests

import (
	"fmt"
	"log"
	"os"
	"testing"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/controllers"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/models"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

var server = controllers.Server{}

var userInstance = models.User{}
var healthDataInstance = models.HealthDetail{}

// TestMain method to run the test for loading env file
func TestMain(m *testing.M) {
	var err error
	err = godotenv.Load(os.ExpandEnv("../../.env"))

	if err != nil {
		log.Fatalf("Error getting env %v\n", err)
	}

	Database()

	os.Exit(m.Run())
}

// Database method for testing the connection to the database
func Database() {
	var err error

	TestDbDriver := os.Getenv("TestDbDriver")

	if TestDbDriver == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("TestDbUser"), os.Getenv("TestDbPassword"), os.Getenv("TestDbHost"), os.Getenv("TestDbPort"), os.Getenv("TestDbName"))

		server.Db, err = gorm.Open(TestDbDriver, DBURL)

		if err != nil {
			fmt.Printf("Cannot connect to database")
			log.Fatalf("There is an error: %v", err)
		} else {
			fmt.Printf("We are connected to the database")
		}
	}
}

// createUserTable method to test the creation of User table
// This method will return the error type variable
func createUserTable() error {
	err := server.Db.DropTableIfExists(&models.User{}).Error

	if err != nil {
		return err
	}

	err = server.Db.AutoMigrate(&models.User{}).Error

	if err != nil {
		return err
	}

	log.Printf("Successfully created User table")
	return nil
}

// seedOneUser method to test the creation of the single user
// This method will return the user object and the error object
func seedOneUser() (models.User, error) {
	createUserTable()

	user := models.User{
		Name:     "Ethan Hunt",
		Email:    "ethan@gmail.com",
		Password: "testers",
	}

	err := server.Db.Model(&models.User{}).Create(&user).Error

	if err != nil {
		log.Fatalf("There is an error in creating the user: %v", err)
	}

	return user, nil
}

// seedUsers method will test the creation of multiple users
func seedUsers() error {
	users := []models.User{
		models.User{
			Name:     "Ethan Hunt",
			Email:    "ethan@gmail.com",
			Password: "testers",
		},
		models.User{
			Name:     "Kabir",
			Email:    "kabir@gmail.com",
			Password: "testers",
		},
	}

	for i, _ := range users {
		err := server.Db.Model(&models.User{}).Create(&users[i]).Error

		if err != nil {
			return err
		}
	}

	return nil
}

// createUserAndHealthDetailTable method will test the creation of user and health detail tables respectively
// This method will return the error type object
func createUserAndHealthDetailTable() error {
	err := server.Db.DropTableIfExists(&models.User{}, &models.HealthDetail{}).Error

	if err != nil {
		return err
	}

	err = server.Db.AutoMigrate(&models.User{}, &models.HealthDetail{}).Error

	if err != nil {
		return err
	}

	log.Printf("Successfully created User and Health Detail tables respectively")

	return nil
}

// seedOneUserAndOneHealthDetail method will test the creation of user and health detail records respectively
// This method will return the health detail object and the error object
func seedOneUserAndOneHealthDetail() (models.HealthDetail, error) {
	err := createUserAndHealthDetailTable()

	if err != nil {
		return models.HealthDetail{}, err
	}

	user := models.User{
		Name:     "Kabir",
		Email:    "kabir@gmail.com",
		Password: "testers",
	}

	err = server.Db.Model(&models.User{}).Create(&user).Error

	if err != nil {
		return models.HealthDetail{}, err
	}

	healthDetail := models.HealthDetail{
		Userid:     user.Id,
		Height:     180,
		Weight:     75,
		Age:        60,
		Bloodgroup: "AB+",
	}

	err = server.Db.Model(&models.HealthDetail{}).Create(&healthDetail).Error

	if err != nil {
		return models.HealthDetail{}, err
	}

	return healthDetail, nil
}

// seedUsersAndHealthDetails method will test the creation of multiple users
func seedUsersAndHealthDetails() ([]models.User, []models.HealthDetail, error) {
	var err error

	if err != nil {
		return []models.User{}, []models.HealthDetail{}, err
	}

	users := []models.User{
		models.User{
			Name:     "Ethan Hunt",
			Email:    "ethan@gmail.com",
			Password: "testers",
		},
		models.User{
			Name:     "Kabir",
			Email:    "kabir@gmail.com",
			Password: "testers",
		},
	}

	healthDetail := []models.HealthDetail{
		models.HealthDetail{
			Height:     180,
			Weight:     75,
			Age:        60,
			Bloodgroup: "AB+",
		},
		models.HealthDetail{
			Height:     150,
			Weight:     70,
			Age:        65,
			Bloodgroup: "B+",
		},
	}

	for i, _ := range users {
		err := server.Db.Model(&models.User{}).Create(&users[i]).Error

		if err != nil {
			log.Fatalf("There is an error in creating the user: %v", err)
		}

		healthDetail[i].Userid = users[i].Id

		healthDetErr := server.Db.Model(&models.HealthDetail{}).Create(&healthDetail[i]).Error

		if healthDetErr != nil {
			log.Fatalf("There is an error adding the health details for the user: %v", err)
		}
	}

	return users, healthDetail, nil
}
