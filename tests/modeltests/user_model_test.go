package modeltests

import (
	"log"
	"testing"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/models"
	"gopkg.in/go-playground/assert.v1"
)

// TestFindAllUsers method will test getting all the users of the system
func TestFindAllUsers(t *testing.T) {
	err := createUserTable()

	if err != nil {
		log.Fatal(err)
	}

	err = seedUsers()

	if err != nil {
		log.Fatal(err)
	}

	users, err := userInstance.FindAllUsers(server.Db)

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, len(*users), 2)

}

// TestSaveUser method test saving the user details
func TestSaveUser(t *testing.T) {
	err := createUserTable()

	if err != nil {
		log.Fatal(err)
	}

	newUser := models.User{
		Name:     "Johny Bravo",
		Email:    "johny@gmail.com",
		Password: "test",
	}

	savedUser, err := newUser.SaveUser(server.Db)

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, newUser.Name, savedUser.Name)
}

//TestGetUserById method test the retrieval of user details by user id
func TestGetUserById(t *testing.T) {
	err := createUserTable()

	if err != nil {
		log.Fatal(err)
	}

	user, err := seedOneUser()

	if err != nil {
		log.Fatal(err)
	}

	userDet, err := userInstance.FindUserbyId(server.Db, user.Id)

	assert.Equal(t, user.Id, userDet.Id)
	assert.Equal(t, user.Name, userDet.Name)
}

//TestUpdateUser method test for updating the user
func TestUpdateUser(t *testing.T) {
	err := createUserTable()

	if err != nil {
		log.Fatal(err)
	}

	newUser, err := seedOneUser()

	if err != nil {
		log.Fatal(err)
	}

	updateUser := models.User{
		Name:     "Ron",
		Email:    "ron@gmail.com",
		Password: "test",
	}

	updateUserDet, err := updateUser.UpdateUser(server.Db, newUser.Id)

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, updateUserDet.Name, updateUser.Name)

}

// TestDeleteUser method tests the deletion of the user
func TestDeleteUser(t *testing.T) {
	err := createUserTable()

	if err != nil {
		log.Fatal(err)
	}

	newUser, err := seedOneUser()

	if err != nil {
		log.Fatal(err)
	}

	rowsAffected, err := userInstance.DeleteUser(server.Db, newUser.Id)

	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, rowsAffected, int64(1))
}
