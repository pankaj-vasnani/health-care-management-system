package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

type HealthDetail struct {
	ID         uint32    `gorm:"primary_key;auto_increment" json:"id"`
	Userid     uint32    `gorm:"not null" json:"user_id"`
	Height     uint32    `gorm:"not null" json:"height"`
	Weight     uint32    `gorm:"not null" json:"weight"`
	Age        uint32    `gorm:"not null" json:"age"`
	Bloodgroup string    `gorm:"size:10;not null" json:"blood_group"`
	Createdat  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	Updatedat  time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

/**
 * Function to prepare the Health Input Data
 */
func (h *HealthDetail) Prepare() {
	h.ID = 0
	h.Userid = 0
	h.Height = h.Height
	h.Weight = h.Weight
	h.Age = h.Age
	h.Bloodgroup = h.Bloodgroup
	h.Createdat = time.Now()
	h.Updatedat = time.Now()
}

/**
 * Function to validate the data
 */
func (h *HealthDetail) Validate() error {
	if h.Weight <= 0 {
		return errors.New("Please enter the weight")
	}

	if h.Height <= 0 {
		return errors.New("Please enter the height")
	}

	if h.Bloodgroup == "" {
		return errors.New("Please select the blood group")
	}

	if h.Age <= 0 {
		return errors.New("Please enter the age")
	}

	return nil
}

func (h *HealthDetail) SaveData(db *gorm.DB) (*HealthDetail, error) {
	var err error

	err = db.Debug().Model(&HealthDetail{}).Create(&h).Error

	if err != nil {
		return &HealthDetail{}, err
	}

	// if h.User_id != 0 {
	// 	err = db.Debug().Model(&HealthDetail{}).Where("child_user_id = ?", h.child_user_id).Take(&h)

	// 	if err.RowsAffected == 1 {
	// 		return &HealthDetail{}, err
	// 	}
	// }

	return h, nil
}

/**
 * Function to retrieve the Health Data by User Id
 */
func (h *HealthDetail) FindHealthDataByUserId(db *gorm.DB, uid uint32) (*HealthDetail, error) {
	var err error

	err = db.Debug().Model(&HealthDetail{}).Where("Userid = ?", uid).Take(&h).Error

	if err != nil {
		return &HealthDetail{}, err
	}

	return h, nil
}

/**
 * Function to retrieve the Details of a particular Health Data by Id
 */
func (h *HealthDetail) FindHealthDetails(db *gorm.DB, id uint32) (*HealthDetail, error) {
	var err error

	err = db.Debug().Model(&HealthDetail{}).Where("Id = ?", id).Take(&h).Error

	if err != nil {
		return &HealthDetail{}, err
	}

	return h, nil
}

/**
 * Function to update the Health Details
 */
func (h *HealthDetail) UpdateHealthDetail(db *gorm.DB, id uint64) (*HealthDetail, error) {
	err := db.Debug().Model(&HealthDetail{}).Where("Id = ?", id).Updates(
		HealthDetail{
			Userid:     h.Userid,
			Weight:     h.Weight,
			Height:     h.Height,
			Age:        h.Age,
			Bloodgroup: h.Bloodgroup,
			Updatedat:  time.Now(),
		},
	).Error

	fmt.Println(err)

	if err != nil {
		return &HealthDetail{}, err
	}

	return h, nil
}
