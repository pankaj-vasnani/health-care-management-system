package models

import (
	"errors"
	"log"
	"strings"
	"time"

	"github.com/badoux/checkmail"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id         uint32    `gorm:"primary_key;auto_increment" json:"id"`
	Name       string    `gorm:"size:255;not null" json:"name"`
	Email      string    `gorm:"size:255;not null;unique" json:"email"`
	Password   string    `gorm:"size:255;not null;" json:"password"`
	Created_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	Updated_at time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func (u *User) BeforeSave() error {
	hashedPassword, err := Hash(u.Password)

	if err != nil {
		return err
	}

	u.Password = string(hashedPassword)

	return nil
}

func (u *User) Prepare() {
	u.Id = 0
	u.Name = strings.TrimSpace(u.Name)
	u.Email = strings.TrimSpace(u.Email)
	u.Created_at = time.Now()
	u.Updated_at = time.Now()
}

func (u *User) Validate(action string) error {
	switch action {
	case "update":
		if u.Name == "" {
			return errors.New("Please enter user name")
		}

		if u.Email == "" {
			return errors.New("Please enter user email address")
		}

		if u.Password == "" {
			return errors.New("Please enter password")
		}

		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Please enter valid email address format")
		}

		return nil

	case "login":
		if u.Email == "" {
			return errors.New("Please enter user email address")
		}

		if u.Password == "" {
			return errors.New("Please enter password")
		}

		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Please enter valid email address")
		}

		return nil

	default:
		if u.Name == "" {
			return errors.New("Please enter user name")
		}

		if u.Email == "" {
			return errors.New("Please enter user email address")
		}

		if u.Password == "" {
			return errors.New("Please enter password")
		}

		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Please enter valid email address")
		}

		return nil
	}
}

func (u *User) SaveUser(db *gorm.DB) (*User, error) {
	var err error

	err = db.Debug().Create(&u).Error

	if err != nil {
		return &User{}, err
	}

	return u, nil
}

func (u *User) FindAllUsers(db *gorm.DB) (*[]User, error) {
	var err error

	users := []User{}

	err = db.Debug().Model(&User{}).Limit(100).Find(&users).Error

	if err != nil {
		return &[]User{}, err
	}

	return &users, err
}

func (u *User) FindUserbyId(db *gorm.DB, uid uint32) (*User, error) {
	var err error

	err = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u).Error

	if err != nil {
		return &User{}, err
	}

	return u, err
}

func (u *User) UpdateUser(db *gorm.DB, uid uint32) (*User, error) {
	// Hash the password

	err := u.BeforeSave()

	if err != nil {
		log.Fatal(err)
	}

	// Find and Update the user
	db = db.Debug().Model(&User{}).Where("Id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"password":   u.Password,
			"name":       u.Name,
			"email":      u.Email,
			"updated_at": time.Now(),
		},
	)

	if db.Error != nil {
		return &User{}, db.Error
	}

	// Return the updated user
	errGetUser := db.Debug().Model(&User{}).Where("Id = ?", uid).Take(&User{}).Error

	if errGetUser != nil {
		return &User{}, errGetUser
	}

	return u, nil
}

func (u *User) DeleteUser(db *gorm.DB, uid uint32) (int64, error) {
	err := db.Debug().Model(&User{}).Where("Id = ?", uid).Take(&User{}).Delete(&User{})

	if err.Error != nil {
		return 0, err.Error
	}

	return int64(1), nil
}
