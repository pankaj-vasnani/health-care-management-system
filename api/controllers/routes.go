package controllers

import "bitbucket.org/pankaj-vasnani/health-care-management-system/api/middlewares"

func (s *Server) initializeRoutes() {
	// Test Route API
	s.Router.HandleFunc("/", middlewares.SetJSONResponse(s.Test)).Methods("POST")

	// Login Route API
	s.Router.HandleFunc("/login", middlewares.SetJSONResponse(s.Login)).Methods("POST")

	// Users routes
	s.Router.HandleFunc("/users", middlewares.SetJSONResponse(s.CreateUser)).Methods("POST")
	s.Router.HandleFunc("/users", middlewares.SetJSONResponse(s.GetAllUsers)).Methods("GET")
	s.Router.HandleFunc("/users/{id}", middlewares.SetJSONResponse(s.GetUser)).Methods("GET")
	s.Router.HandleFunc("/users/{id}", middlewares.SetJSONResponse(middlewares.SetAuthentication(s.UpdateUser))).Methods("PUT")
	s.Router.HandleFunc("/users/{id}", middlewares.SetAuthentication(s.DeleteUser)).Methods("DELETE")

	// Health Detail Route API
	s.Router.HandleFunc("/health_data", middlewares.SetJSONResponse(s.addHealthData)).Methods("POST")
	s.Router.HandleFunc("/health_data", middlewares.SetJSONResponse(s.GetAllHealthData)).Methods("GET")
	s.Router.HandleFunc("/health_data/{id}", middlewares.SetJSONResponse(s.GetHealthDetails)).Methods("GET")
	s.Router.HandleFunc("/health_data/{id}", middlewares.SetJSONResponse(middlewares.SetAuthentication(s.UpdateHealthDetails))).Methods("PUT")
}
