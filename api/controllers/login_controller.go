package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/auth"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/models"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/responses"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/utils/formaterror"
	"golang.org/x/crypto/bcrypt"
)

// Login method is used to login the user into the system
// It returns the newly created user token
func (server *Server) Login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user := models.User{}
	err = json.Unmarshal(body, &user)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user.Prepare()
	err = user.Validate("login")

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	token, err := server.SignIn(user.Email, user.Password)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusUnprocessableEntity, formattedError)
		return
	}

	responses.JSON(w, http.StatusOK, token)

}

// SignIn method is used to create the login token for the user to login into the system
// It creates and returns the token to the above Login method to enable the user to login into the system
func (server *Server) SignIn(email, password string) (string, error) {
	var err error

	user := models.User{}

	err = server.Db.Debug().Model(models.User{}).Where("email = ?", email).Take(&user).Error

	if err != nil {
		return "", err
	}

	err = models.VerifyPassword(user.Password, password)

	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}

	return auth.CreateToken(int32(user.Id))
}
