// Package controllers provides the business side logic for managing
// the users and health data for the Health Care Management System
package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/auth"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/models"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/responses"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/utils/formaterror"
	"github.com/gorilla/mux"
)

// addHealthData method saves the health data for the logged in user
// It returns the health data for the logged in user
func (server *Server) addHealthData(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	healthData := models.HealthDetail{}
	err = json.Unmarshal(body, &healthData)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	healthData.Prepare()
	err = healthData.Validate()

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	uid, err := auth.ExtractTokenId(r)

	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	if uid != healthData.Userid {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	// Save the Health Data
	healthDataSaved, err := healthData.SaveData(server.Db)

	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, healthDataSaved.ID))

	responses.JSON(w, http.StatusOK, healthDataSaved)
}

// GetAllHealthData method returns all the health related data for
// the logged in user
// It returns the health data of the logged in user
func (server *Server) GetAllHealthData(w http.ResponseWriter, r *http.Request) {
	uid, err := auth.ExtractTokenId(r)

	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	healthDetail := models.HealthDetail{}

	healthData, err := healthDetail.FindHealthDataByUserId(server.Db, uid)

	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, healthData)
}

// GetHealthDetails method returns the details of a particular health data of the logged in user
// It returns the details of the particular health data of the logged in user
func (server *Server) GetHealthDetails(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)

	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	healthDetail := models.HealthDetail{}

	healthDetails, err := healthDetail.FindHealthDetails(server.Db, uint32(id))

	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, healthDetails)
}

// UpdateHealthDetails method updates the details of a particular health data of the logged in user
// It updates and returns the details of the particular health data of the logged in user
func (server *Server) UpdateHealthDetails(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseUint(vars["id"], 10, 32)

	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	//CHeck if the auth token is valid and  get the user id from it
	uid, authErr := auth.ExtractTokenId(r)
	if authErr != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	// Check if the post exist
	healthDetails := models.HealthDetail{}
	err = server.Db.Debug().Model(models.HealthDetail{}).Where("id = ?", id).Take(&healthDetails).Error

	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("Health Data not found"))
		return
	}

	// If a user attempt to update a post not belonging to him
	if uid != healthDetails.Userid {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	healthDataUpdate := models.HealthDetail{}
	err = json.Unmarshal(body, &healthDataUpdate)

	// log.Fatalf("%v", healthDataUpdate)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Update the data
	healthDataUpdate.Prepare()
	err = healthDataUpdate.Validate()

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	healthDataUpdate.ID = healthDetails.ID //this is important to tell the model the post id to update, the other update field are set above

	healthDataUpdated, err := healthDataUpdate.UpdateHealthDetail(server.Db, id)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}

	responses.JSON(w, http.StatusOK, healthDataUpdated)
}
