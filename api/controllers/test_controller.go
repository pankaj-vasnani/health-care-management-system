package controllers

import (
	"net/http"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/responses"
)

func (server *Server) Test(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Welcome to the Health Care Management System")
}
