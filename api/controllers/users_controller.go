// Package controllers provides the business side logic for managing
// the users and health data for the Health Care Management System
package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/auth"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/responses"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/utils/formaterror"

	"github.com/gorilla/mux"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/models"
)

// CreateUser method creates a new user
// It returns the new user
func (server *Server) CreateUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}

	user := models.User{}

	err = json.Unmarshal(body, &user)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}

	user.Prepare()

	err = user.Validate("")

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)

		return
	}

	// Save the data
	userCreated, err := user.SaveUser(server.Db)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())

		responses.ERROR(w, http.StatusInternalServerError, formattedError)

		return
	}

	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, userCreated.Id))

	responses.JSON(w, http.StatusCreated, userCreated)

}

// GetAllUsers method gets all the users from the system
// It returns all the users from the system
func (server *Server) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	var user = models.User{}

	users, err := user.FindAllUsers(server.Db)

	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	// Return all the users
	responses.JSON(w, http.StatusOK, users)
}

// GetUser method returns the single user corresponding to the particular id
// It returns the single user according to a particular id
func (server *Server) GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["id"], 10, 32)

	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	var user = models.User{}

	singleUser, err := user.FindUserbyId(server.Db, uint32(uid))

	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	responses.JSON(w, http.StatusOK, singleUser)

}

// UpdateUser method updates the user
// It returns the updated user details
func (server *Server) UpdateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	uid, err := strconv.ParseUint(vars["id"], 10, 32)

	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	users := models.User{}
	err = json.Unmarshal(body, &users)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	tokenId, err := auth.ExtractTokenId(r)

	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized Request"))
		return
	}

	if tokenId != uint32(uid) {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	users.Prepare()

	err = users.Validate("update")

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	updatedUser, err := users.UpdateUser(server.Db, uint32(uid))

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}

	responses.JSON(w, http.StatusOK, updatedUser)
}

// DeleteUser method deletes the user
// It returns the blank content indicating that the user has been deleted
func (server *Server) DeleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user := models.User{}
	uid, err := strconv.ParseUint(vars["id"], 10, 32)

	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	tokenId, err := auth.ExtractTokenId(r)

	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized access"))
		return
	}

	if tokenId != 0 && tokenId != uint32(uid) {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	_, errDelPerform := user.DeleteUser(server.Db, uint32(uid))

	if errDelPerform != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusNoContent, "")
}
