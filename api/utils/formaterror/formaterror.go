package formaterror

import (
	"errors"
	"strings"
)

func FormatError(err string) error {
	if strings.Contains(err, "name") {
		return errors.New("This name has already been taken by another user. Please enter new name")
	}

	if strings.Contains(err, "email") {
		return errors.New("This email has been taken by another user")
	}

	if strings.Contains(err, "hashedPassword") {
		return errors.New("Incorrect password")
	}

	return errors.New("Incorrect Details")
}
