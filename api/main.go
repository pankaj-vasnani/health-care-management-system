package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/controllers"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/seed"
	"github.com/joho/godotenv"
)

func main() {
	var server = controllers.Server{}

	var err error

	err = godotenv.Load("../.env")

	if err != nil {
		log.Fatalf("Error getting env: %v", err)
	} else {
		fmt.Println("We are getting the env values")
	}

	server.Initialize(os.Getenv("DB_DRIVER"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_PORT"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))

	seed.Load(server.Db)

	server.Run(":8080")
}
