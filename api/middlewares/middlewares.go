package middlewares

import (
	"errors"
	"net/http"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/auth"
	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/responses"
)

func SetJSONResponse(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application-json")

		next(w, r)
	}
}

func SetAuthentication(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := auth.CheckTokenValidity(r)

		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized access"))

			return
		}

		next(w, r)
	}
}
