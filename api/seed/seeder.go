package seed

import (
	"log"

	"bitbucket.org/pankaj-vasnani/health-care-management-system/api/models"
	"github.com/jinzhu/gorm"
)

var users = []models.User{
	models.User{
		Name:     "Pankaj Ramesh Vasnani",
		Email:    "pankaj.techuser@gmail.com",
		Password: "test",
	},
	models.User{
		Name:     "Jack Sparrow",
		Email:    "jack.pirateuser@gmail.com",
		Password: "tester",
	},
}

var health_data = []models.HealthDetail{
	models.HealthDetail{
		Height:     60,
		Weight:     75,
		Age:        28,
		Bloodgroup: "O+",
	},
	models.HealthDetail{
		Height:     60,
		Weight:     56,
		Age:        50,
		Bloodgroup: "A+",
	},
}

func Load(db *gorm.DB) {
	err := db.Debug().DropTableIfExists(&models.HealthDetail{}, &models.User{}).Error

	if err != nil {
		log.Fatal("Cannot drop table: %v", err)
	}

	// Migration of tables
	err = db.Debug().AutoMigrate(&models.HealthDetail{}, &models.User{}).Error

	if err != nil {
		log.Fatal("Cannot migrate the tables: %v", err)
	}

	err = db.Debug().Model(&models.HealthDetail{}).AddForeignKey("userid", "users(id)", "cascade", "cascade").Error

	if err != nil {
		log.Fatal("Error making the foreign key : %v", err)
	}

	for i, _ := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error

		if err != nil {
			log.Fatal("Error creating new user: %v", err)
		}

		health_data[i].Userid = users[i].Id

		err = db.Debug().Model(&models.HealthDetail{}).Create(&health_data[i]).Error

		if err != nil {
			log.Fatal("Error creating new health data: %v", err)
		}
	}
}
